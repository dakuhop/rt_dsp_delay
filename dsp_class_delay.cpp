#include "RtAudio.h"
#include <iostream>
#include <cstdlib>
#include <stdio.h>

typedef signed short MY_TYPE;
#define FORMAT RTAUDIO_FLOAT64

// Platform-dependent sleep routines.
#if defined( __WINDOWS_ASIO__ ) || defined( __WINDOWS_DS__ ) || defined( __WINDOWS_WASAPI__ )
  #include <windows.h>
  #define SLEEP( milliseconds ) Sleep( (DWORD) milliseconds )
#else
  #include <unistd.h>
  #define SLEEP( milliseconds ) usleep( (unsigned long) (milliseconds * 1000.0) )
#endif

float delay_time, gain_dry, gain_wet;
int delay_samples;
double** delay_array;
int* counter;

// ================================================================================
int audioCallback(void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
		  double streamTime, RtAudioStreamStatus status, void* data)
{
    double* oBuffer = (double*)outputBuffer;
    double* iBuffer = (double*)inputBuffer;
    double* lastValues = (double*)data;

    int* offsetted_counter = new int[2];

    for (int frameIndex = 0; frameIndex < nBufferFrames; frameIndex++) {
	for (int channel = 0; channel < 2; channel++) {
	    *oBuffer++ = lastValues[channel];

	    lastValues[channel] = *iBuffer;
	    offsetted_counter[channel] = (counter[channel]) % delay_samples;
	    double delay_sample = delay_array[channel][offsetted_counter[channel]];
	    delay_array[channel][counter[channel]] = lastValues[channel];
	    lastValues[channel] = (lastValues[channel] * gain_dry) + (delay_sample * gain_wet);

	    counter[channel] = (counter[channel] + 1) % delay_samples;
	}
	iBuffer++;
    }

    return 0;
}

// ================================================================================
int main()
{
    RtAudio adac;

    if (adac.getDeviceCount() < 1) {
	std::cout << "\nNo audio devices found!" << "\n";
	exit(0);
    }
    
    // Output-Parameter
    RtAudio::StreamParameters iParams, oParams;
    iParams.deviceId = adac.getDefaultInputDevice();
    iParams.nChannels = 1;
    oParams.deviceId = adac.getDefaultOutputDevice();
    oParams.nChannels = 2;

    unsigned int sampleRate = 44100;
    unsigned int bufferFrames = 256; // 1 frame = samples fuer alle kanaele

    std::cout << "delay time?" << "\n";
    std::cin >> delay_time;
    delay_samples = delay_time * sampleRate;
    delay_array = new double*[oParams.nChannels];
    counter = new int[oParams.nChannels];
    for (int channel = 0; channel < oParams.nChannels; channel++) {
	delay_array[channel] = new double[delay_samples]; // set array into array
        counter[channel] = 0; // set the two counters to 0
        for (int sample_index = 0; sample_index < delay_samples; sample_index++)
	    delay_array[channel][sample_index] = 0; // write all init samples to 0
    }
    gain_dry = 0.9;
    gain_wet = 0.7;

    double data[2];

    // unsigned int bufferBytes, bufferFrames = 512;

    try {
	adac.openStream(&oParams, &iParams, FORMAT, sampleRate,
			&bufferFrames, &audioCallback, (void*)&data);
	// dac.startStream();
    }
    catch (RtAudioError& e) {
	e.printMessage();
	exit(0);
    }

    // bufferBytes = bufferFrames * 2 * 4;
    try {
	adac.startStream();
	char input;
	std::cout << "\nPlaying ... press <enter> to quit." << "\n";
	std::cin.get(input);
	std::cin.get(input);
	adac.stopStream();
    }
    catch (RtAudioError& e) {
	e.printMessage();
	goto cleanup;
    }
 cleanup:
    if (adac.isStreamOpen()) adac.closeStream();

    return 0;
}
